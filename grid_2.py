def setup(): #once
    size(600, 600)
    background(255)
    #frameRate(12)
    #noLoop() # stop animation
    rectMode(CENTER)

def draw():
    noStroke()

    size = 60

    for j in range(10):
        y = j * size  + size / 2

        for i in range(10):
            x = i * size  + size / 2

            fill_var = fill(random(200))
            size_var = random(10, 20)

            if j <= i or i >= j:
              size_var += 20

            rect(x, y, size_var, size_var)