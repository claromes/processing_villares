def setup(): #once
    size(600, 600)
    background(200, 255, 10)
    #frameRate(12)

    tab_size = 60
    pos_y = 100

    fill(255, 25, 250)
    textSize(24)
    noStroke()

    fruits = ["apple", "grape", "banana", "orange"]

    for f in fruits:
        text(f, tab_size, pos_y)
        pos_y += 60

    for i in range(1, 20, 4):
        text(i, tab_size, pos_y)
        pos_y += 20
