def setup(): #once
    size(600, 600)
    background(255)
    #frameRate(12)
    #noLoop() # stop animation
    rectMode(CENTER)

def draw():
    noStroke()
    background(0, random(200), 0, 50)
    random_size = random(10, 60)

    size = 60

    for j in range(10):
        y = j * size  + size / 2
        #random_size = random(10, 60)

        for i in range(10):
            x = i * size  + size / 2

            fill(0, random(200), 0, 50)
            #random_size = random(10, 60)
            rect(x, y, random_size, random_size)

    if frameCount > 24: # draw() animation limit
        noLoop()